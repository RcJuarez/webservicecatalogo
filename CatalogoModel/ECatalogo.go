package CatalogoModel

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//servicioWeb contiene la url del servicio rest
var servicioWeb string

//cabecera Cabecera del documento a enviar/recibir
type Cabecera struct {
	ModuloOrigen  string
	ModuloDestino string
	Fecha         time.Time
	ServicioWeb   string
}

//Estructura del documento a enviar
type DocumentoEntrada struct {
	Cabecera            Cabecera
	InformacionCatalogo InformacionCatalogo
}

type InformacionCatalogo struct {
	ID               bson.ObjectId `bson:"_id,omitempty"`
	Clave            int           `bson:"Clave"`
	Nombre           string        `bson:"Nombre"`
	Descripcion      string        `bson:"Descripcion"`
	CatalogoEditable bool          `bson:"Editable"`
	EstatusCatalogo  string        `bson:"Estatus"`
	Elementos        []elementoMgo `bson:"Elementos"`
	ElementoDefault  string        `bson:"ElementoDefault,omitempty"`
}

//ElementoMgo subestructura de CatalogoNuevo
type elementoMgo struct {
	SubClave      string `bson:"SubClave"`
	Valor         string `bson:"Valor"`
	ValorEditable bool   `bson:"Editabe"`
	EstatusValor  string `bson:"Estatus"`
}

////////////////////////////////////
//Estructura del documento a recibir
///////////////////////////////////
type ControlPeticion struct {
	Estatus bool
	Mensaje string
	Err     error
}

type DocumentoSalida struct {
	Checksum       [16]byte
	Cabecera       *Cabecera
	ObjetoCatalogo *[]ObjetoCatalogo
}

type ObjetoCatalogo struct {
	ControlPeticion     *ControlPeticion
	EstID               *EstID
	EstClave            *EstClave
	EstNombre           *EstNombre
	EstDescripcion      *EstDescripcion
	EstCatalogoEditable *EstCatalogoEditable
	EstEstatusCatalogo  *EstEstatusCatalogo
	Elementos           *[]EstElementoMgo
	EstElementoDefault  *EstElementoDefault
	EstBitacora         *[]BitacoraElastic
}

//Estructuras con estatus detallados
type EstID struct {
	ID              bson.ObjectId `bson:"_id,omitempty"`
	ControlPeticion *ControlPeticion
}

type EstClave struct {
	Clave           int `bson:"Clave"`
	ControlPeticion *ControlPeticion
}

type EstNombre struct {
	Nombre          string `bson:"Nombre"`
	ControlPeticion *ControlPeticion
}

type EstDescripcion struct {
	Descripcion     string `bson:"Descripcion"`
	ControlPeticion *ControlPeticion
}

type EstCatalogoEditable struct {
	CatalogoEditable bool `bson:"Editable"`
	ControlPeticion  *ControlPeticion
}

type EstEstatusCatalogo struct {
	EstatusCatalogo string `bson:"Estatus"`
	ControlPeticion *ControlPeticion
}

type EstElementoDefault struct {
	ElementoDefault bson.ObjectId `bson:"ElementoDefault,omitempty"`
	ControlPeticion *ControlPeticion
}

//ElementoMgo subestructura de CatalogoNuevo
type EstElementoMgo struct {
	EstSubClave      EstSubClave
	EstValor         EstValor
	EstValorEditable EstValorEditable
	EstEstatusValor  EstEstatusValor
}

type EstSubClave struct {
	SubClave        string `bson:"SubClave"`
	ControlPeticion *ControlPeticion
}

type EstValor struct {
	Valor           string `bson:"Valor"`
	ControlPeticion *ControlPeticion
}

type EstValorEditable struct {
	ValorEditable   string `bson:"Editabe"`
	ControlPeticion *ControlPeticion
}

type EstEstatusValor struct {
	EstatusValor    string `bson:"Estatus"`
	ControlPeticion *ControlPeticion
}

//#################CATALOGO PARA ELASTICSEARCH############################3

//CatalogoNuevoElastic estructura de CatalogoNuevos para insertar en Elastic
type CatalogoElastic struct {
	Clave            int               `json:"Clave"`
	Nombre           string            `json:"Nombre"`
	Descripcion      string            `json:"Descripcion"`
	CatalogoEditable bool              `json:"Editable"`
	EstatusCatalogo  string            `json:"Estatus"`
	Elementos        []ElementoElastic `json:"Elementos"`
	Bitacora         []BitacoraElastic `json:"Bitacora"`
}

//ElementoElastic subestructura de CatalogoNuevo
type ElementoElastic struct {
	SubClave      string `json:"SubClave"`
	Valor         string `json:"Valor"`
	ValorEditable bool   `json:"Editabe"`
	EstatusValor  string `json:"Estatus"`
}

//BitacoraElastic subestructura de CatalogoNuevo
type BitacoraElastic struct {
	Fecha     time.Time        `json:"Fecha,omitempty"`
	IDUsuario bson.ObjectId    `json:"IDUsuario,omitempty"`
	Concepto  string           `json:"Concepto,omitempty"`
	Cambios   []CambiosElastic `json:"Cambios"`
}

//CambiosElastic subestructura de CatalogoNuevo
type CambiosElastic struct {
	ElementoPrevio string `json:"ElementoPrevio,omitempty"`
	ElementoNuevo  string `json:"ElementoNuevo,omitempty"`
}
