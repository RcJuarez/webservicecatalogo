package CatalogoModel

import (
	"../Modulos/Variables"
)

/*
ICatalogo Interfaz que contiene los métodos CRUD para el catalogo.

Autor: Ramón Cruz Juárez
FechaCreacion: 26/09/2017
FechaModificacion: --- : ---
Modificacion : ---
*/
type ICatalogo interface {
	CrearCatalogoMongo(alm *InformacionCatalogo) error
	CrearCatalogoElastic(alm *InformacionCatalogo) error

	ObtenerCatalogosMongo() (*[]InformacionCatalogo, error)
}

//DataM es una estructura que contiene los datos de configuración en el archivo cfg
var DataM = MoVar.CargaSeccionCFG(MoVar.SecMongo)

//################################################<<METODOS DE GESTION >>################################################################

var iCatalogo ICatalogo

/*
EstablecerCatalogo asigna la interfaz a una variable local del sistema
Requiere la interfaz de un Catalogo.
Regresa ningun objeto

Autor: Ramón Cruz Juárez
FechaCreacion: 14/09/2017
FechaModificacion: --- : Programador
Modificacion : ---
*/
func EstablecerCatalogo(alm ICatalogo) {
	iCatalogo = alm
}

//##################################<< INSERTAR >>###################################

/*
CrearCatalogoMongo redirecciona la informacion del Catalogo par ser guardado en mongodb.
Requiere ningun objeto
Regresa un error si elmacen no se pudo crear correctamente

Autor: Ramón Cruz Juárez
FechaCreacion: 14/09/2017
FechaModificacion: --- : Programador
Modificacion : ---
*/
func (u *InformacionCatalogo) CrearCatalogoMongo() error {
	return iCatalogo.CrearCatalogoMongo(u)
}

/*
ObtenerCatalogosMongo redirecciona a la interfaz de catalogo para consultar la base de datos
Requiere ningun objeto
Regresa el conjunto de catalogos encontrados, o error en caso de que no exista

Autor: Ramón Cruz Juárez
FechaCreacion: 24/09/2017
FechaModificacion: --- : Programador
Modificacion : ---
*/
func (u *InformacionCatalogo) ObtenerCatalogosMongo() (*[]InformacionCatalogo, error) {
	return iCatalogo.ObtenerCatalogosMongo()
}

/*
CrearCatalogoElastic redirecciona la informacion del Catalogo par ser guardado en elastic.
Requiere ningun objeto
Regresa un error si elmacen no se pudo crear correctamente

Autor: Ramón Cruz Juárez
FechaCreacion: 14/09/2017
FechaModificacion: --- : Programador
Modificacion : ---
*/
func (u *InformacionCatalogo) CrearCatalogoElastic() error {
	return iCatalogo.CrearCatalogoElastic(u)
}
