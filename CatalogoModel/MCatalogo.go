package CatalogoModel

//#########################< FUNCIONES GENERALES MGO >###############################

/*
NuevoAlmacenMgo regresa la referencia a la estructura del catalogo de mongo
Requiere ningun objeto
Regresa un apuntador hacia el catalogo para mongo

Autor: Ramón Cruz Juárez
FechaCreacion: 26/09/2017
FechaModificacion: --- : ---
Modificacion : --
*/
func NuevoAlmacenMgo() *InformacionCatalogo {
	return &InformacionCatalogo{}
}

//#########################< FUNCIONES GENERALES MGO >###############################
