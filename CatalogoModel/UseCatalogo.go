package CatalogoModel

import (
	"fmt"

	"../Modulos/Conexiones"
	"../Modulos/Variables"
	mgo "gopkg.in/mgo.v2"
)

/*
CatalogoContexto estructura que contendrá una colección y un contexto a usar para el objeto

Autor: Ramón Cruz Juárez
FechaCreacion: 14/09/2017
FechaModificacion: --- : Programador
Modificacion : ---
*/
type CatalogoContexto struct {
	Coleccion *mgo.Collection
	Contexto  *MoConexion.Contexto
}

/*
EstablecerContexto crea el contexto con la colección a utilizar
Requiere ningun objeto
Regresa ningun objeto

Autor: Ramón Cruz Juárez
FechaCreacion: 14/09/2017
FechaModificacion: --- : Programador
Modificacion : ---
*/
func (almContx *CatalogoContexto) establecerContexto() error {
	ctx, err := MoConexion.NuevoContexto()
	if err == nil {
		almContx.Contexto = ctx
		almContx.Coleccion = almContx.Contexto.ObtenerColeccion(DataM.NombreBase, MoVar.ColeccionCatalogo)
		//fmt.Println("Nombre de la base de datos: ", DataM.NombreBase)
		//Corroborar la lectura correcta del archivo de configuracion
		//almContx.Coleccion = almContx.Contexto.ObtenerColeccion(MoVar.NombreBaseDatos, MoVar.ColeccionCatalogo)
	}
	return err
}

/*
CrearCatalogoMongo crea un Catalogo en la base de datos MOngodb
Requiere la referencia al Catalogo a ser creado
Regresa error en caso de que no se pudo guardar el Catalogo en Mongo

Autor: Ramón Cruz Juárez
FechaCreacion: 14/09/2017
FechaModificacion: --- : Programador
Modificacion : ---
*/
func (almContx CatalogoContexto) CrearCatalogoMongo(alm *InformacionCatalogo) error {
	err := almContx.establecerContexto()
	if err == nil {
		defer almContx.Contexto.Sesion.Close()
		return almContx.Coleccion.Insert(alm)
	}
	return err
}

/*
ObtenerCatalogosMongo consulta en la base de datos de mongo todos los catalogos existentes
Requiere ningun objeto
Regresa un arreglo de catalogos o error en caso de no encontrarlos

Autor: Ramón Cruz Juárez
FechaCreacion: 24/09/2017
FechaModificacion: --- : Programador
Modificacion : ---
*/
func (almContx CatalogoContexto) ObtenerCatalogosMongo() (*[]InformacionCatalogo, error) {
	cats := &[]InformacionCatalogo{}
	err := almContx.establecerContexto()
	if err == nil {
		defer almContx.Contexto.Sesion.Close()
		err = almContx.Coleccion.Find(nil).All(cats)
	}
	return cats, err
}

/*
CrearCatalogoElastic crea un Catalogo en el servidor de ElasticSearch
Requiere la referencia al Catalogo a ser creado
Regresa error en caso de que no se pudo guardar el Catalogo en Elastic

Autor: Ramón Cruz Juárez
FechaCreacion: 14/09/2017
FechaModificacion: --- : Programador
Modificacion : ---
*/
func (almContx CatalogoContexto) CrearCatalogoElastic(alm *InformacionCatalogo) error {
	var err error
	var CatalogoE *CatalogoElastic
	CatalogoE, err = preparaDatosElastic(alm)
	//Falta mejorar la funcion insertar en elastic, cambiarlo de tal manera que devuelva un error
	insert := MoConexion.InsertaElastic(MoVar.TipoCatalogo, alm.ID.Hex(), CatalogoE)
	if !insert {
		fmt.Println("Error al insertar Catalogo en Elastic")
	}
	return err
}

/*
preparaDatosElastic Parsea el Catalogo de tal manera que sea legible por elastic
Requiere la informacion del Catalogo
Regresa una estructura compatible con elastic y un error en caso de que no se inserte adecuadamente

Autor: Ramón Cruz Juárez
FechaCreacion: 15/06/2017
FechaModificacion: 14/07/2017 : Ramón Cruz Juárez
Modificacion : Ya no será de un tipo de almacén, ahora la información lo solicita por parámetro
*/
func preparaDatosElastic(p *InformacionCatalogo) (*CatalogoElastic, error) {
	var CatalogoNuevoE *CatalogoElastic

	CatalogoNuevoE.Clave = p.Clave
	CatalogoNuevoE.Nombre = p.Nombre
	CatalogoNuevoE.Descripcion = p.Descripcion
	CatalogoNuevoE.CatalogoEditable = p.CatalogoEditable
	CatalogoNuevoE.EstatusCatalogo = p.EstatusCatalogo

	var elementosMongo = p.Elementos
	var elementosElastic = CatalogoNuevoE.Elementos
	for _, v := range elementosMongo {
		var elementoElastic ElementoElastic
		elementoElastic.SubClave = v.SubClave
		elementoElastic.ValorEditable = v.ValorEditable
		elementoElastic.EstatusValor = v.EstatusValor
		elementoElastic.Valor = v.Valor
		elementosElastic = append(elementosElastic, elementoElastic)
	}
	/*
		var bitacorasMongo = p.Bitacora
		var bitacorasElastic = CatalogoNuevoE.Bitacora
		for _, v := range bitacorasMongo {
			var bitacoraElastic BitacoraElastic
			bitacoraElastic.Concepto = v.Concepto
			bitacoraElastic.Fecha = v.Fecha
			bitacoraElastic.IDUsuario = v.IDUsuario
			var cambiosMongo = v.Cambios
			var cambiosElastic = bitacoraElastic.Cambios
			for _, y := range cambiosMongo {
				var cambioElastic CambiosElastic
				cambioElastic.ElementoNuevo = y.ElementoNuevo
				cambioElastic.ElementoPrevio = y.ElementoPrevio
				cambiosElastic = append(cambiosElastic, cambioElastic)
			}
			bitacorasElastic = append(bitacorasElastic, bitacoraElastic)
		}
	*/
	return CatalogoNuevoE, nil
}
