package MoConexion

//Establecece un contexto que tendrá la sesion de MongoDb a utilizar
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 13/09/2017

import (
	mgo "gopkg.in/mgo.v2"
)

//Context Contiene la referencia a una session de mongo
type Contexto struct {
	Sesion *mgo.Session
}

//AlmacenContexto estructura que contendrá una colección y un contexto a usar para una sesion
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 14/09/2017
//---Modificacion:---:---
type ObjetoContexto struct {
	Coleccion *mgo.Collection
	Contexto  *Contexto
}

//Close Accede al contexto y cierra la sesion del mismo
//---Entrada:---:---
//---Salida:---:---
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 13/09/2017
//---Modificacion:---:---
func (c *Contexto) Cerrar() {
	c.Sesion.Close()
}

//ObtenerColeccion Regresa el contenido de una coleccion
//---Entrada: nombreColeccion : nombre de la coleccion a consultar en Mongo
//---Salida: *mgo.Collection :referencia de la colección con los datos solicitados
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 13/09/2017
//---Modificacion:---:---
func (c *Contexto) ObtenerColeccion(nombreBD, nombreColeccion string) *mgo.Collection {
	return c.Sesion.DB(nombreBD).C(nombreColeccion)
}

//NuevoContexto Obtiene la copia de una sesion y lo regresa en un contexto
//---Entrada: --- : ---
//---Salida: *Context :referencia al contexto solicitado
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 13/09/2017
//---Modificacion:---:---
func NuevoContexto() (*Contexto, error) {
	sesion, err := getSession()
	if err == nil {
		sesion = sesion.Copy()
	}
	contexto := &Contexto{
		Sesion: sesion,
	}
	return contexto, err
}
