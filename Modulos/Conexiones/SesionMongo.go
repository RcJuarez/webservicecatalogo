package MoConexion

//Se realiza las conexiones a mongodb
//Tiene los metodos crear, obtener e inicializar las sesiones
//Los almacena en la variable session
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 13/09/2017

import (
	"fmt"

	"gopkg.in/mgo.v2"
)

//session Contendrá la referencia a una sesion de mongodb
var session *mgo.Session

//createDBSession Crea una sesion de mongodb y la asigna a la variable session
//---Entrada:---:---
//---Salida:---:---
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 13/09/2017
//---Modificacion:---:---
func createDBSession() error {
	var err error
	session, err = mgo.Dial("localhost")
	if err != nil {
		fmt.Println("-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_")
		fmt.Println("Error al obtener la sesion: ", err)
		fmt.Println("_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+")
	}
	return err
}

//getSession Obtiene una sesión de MongoDb
//---Entrada:---:---
//---Salida:---:---
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 13/09/2017
//---Modificacion:---:---
func getSession() (*mgo.Session, error) {
	var err error
	if session == nil {
		err = createDBSession()
	}
	return session, err
}

//InitData Inicializa una sesion de MongoDb
//---Entrada:---:---
//---Salida:---:---
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 13/09/2017
//---Modificacion:---:---
func InitData() {
	createDBSession()
}
