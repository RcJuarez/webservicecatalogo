package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"./CatalogoModel"
	"./Modulos/Conexiones"
	"github.com/emicklei/go-restful"
	"gopkg.in/mgo.v2/bson"
)

func main_test() {
	fmt.Println("Servicio Catalogo en escucha: puerto 1025")
	restful.Add(New())
	log.Fatal(http.ListenAndServe(":1025", nil))
}

func New() *restful.WebService {
	service := new(restful.WebService)
	service.
		Path("/Catalogos").
		Consumes(restful.MIME_JSON, restful.MIME_JSON).
		Produces(restful.MIME_JSON, restful.MIME_JSON)

	service.Route(service.GET("/").To(obtenerCatalogos).Doc("Obtener todos los catalogos"))
	service.Route(service.GET("/{doc}").To(obtenerCatalogo).Doc("Obtener solo un catalogo"))
	service.Route(service.POST("").To(crearCatalogo))
	/*
		service.Route(service.GET("/{user-id}").To(FindUser))
		service.Route(service.POST("").To(UpdateUser))
		service.Route(service.PUT("/{user-id}").To(CreateUser))
		service.Route(service.DELETE("/{user-id}").To(RemoveUser))
	*/
	return service
}

func obtenerCatalogos(request *restful.Request, response *restful.Response) {
	docEnt, err := obtenerInformacion(request)
	if err != nil {
		fmt.Println("xxxxxxxxxxxxxxxxx", err, docEnt)
		//response.WriteError(http.StatusInternalServerError, err)
	}

	//Inicializar la base de datos
	MoConexion.InitData()
	//Establecer el contexto a usar
	CatalogoModel.EstablecerCatalogo(CatalogoModel.CatalogoContexto{})
	//Crear un nuevo contexto
	ctx, err := MoConexion.NuevoContexto()
	//validar el error del contexto
	if err != nil {
		fmt.Println("Ocurrieron errores al crear un nuevo contexto", err)
	} else {
		//Cerrar la copia de la conexion
		defer ctx.Cerrar()
	}
	//Crear la referencia del catalogo (paa acceder a los metodos de la interfaz)
	ctlg := &CatalogoModel.InformacionCatalogo{}
	//Acceder al método para obtener los catalogos
	catalogos, err := ctlg.ObtenerCatalogosMongo()
	if err != nil {
		fmt.Println("Error al consultar los catalogos", err)
	} else {
		fmt.Println("Catalogos encontrados: ", catalogos)
	}

	//Rellenar el documento de respuesta

	//Establecer la cabecera
	cab := &CatalogoModel.Cabecera{
		ModuloOrigen:  "Catalogo",
		ModuloDestino: docEnt.Cabecera.ModuloOrigen,
		Fecha:         time.Now(),
		ServicioWeb:   "/localhost:1025/Catalogos",
	}

	//Crear una referencia a la estructura de documento salida
	docSal := &CatalogoModel.DocumentoSalida{}
	//Asignar la cabecera
	docSal.Cabecera = cab
	//Asignar la encriptacion
	docSal.Checksum = generarEncriptacion(docSal.Cabecera)
	//Crear el control de la peticion del objeto
	ctrlpet := &CatalogoModel.ControlPeticion{
		Estatus: true,
		Mensaje: "Exitoso",
		Err:     nil,
	}
	//Crear un arreglo de catalogos
	ObjetosCatalogos := []CatalogoModel.ObjetoCatalogo{}
	//Tratar los datos extraidos
	for _, ctls := range *catalogos {
		ObjetoCatalogo := CatalogoModel.ObjetoCatalogo{
			ControlPeticion: ctrlpet,
			EstID: &CatalogoModel.EstID{
				ID:              ctls.ID,
				ControlPeticion: ctrlpet,
			},
			EstClave: &CatalogoModel.EstClave{
				Clave:           ctls.Clave,
				ControlPeticion: ctrlpet,
			},
			EstNombre: &CatalogoModel.EstNombre{
				Nombre:          ctls.Nombre,
				ControlPeticion: ctrlpet,
			},
			EstDescripcion: &CatalogoModel.EstDescripcion{
				Descripcion:     ctls.Descripcion,
				ControlPeticion: ctrlpet,
			},
			EstCatalogoEditable: &CatalogoModel.EstCatalogoEditable{
				CatalogoEditable: ctls.CatalogoEditable,
				ControlPeticion:  ctrlpet,
			},
			EstEstatusCatalogo: &CatalogoModel.EstEstatusCatalogo{
				EstatusCatalogo: ctls.EstatusCatalogo,
				ControlPeticion: ctrlpet,
			},
			EstElementoDefault: &CatalogoModel.EstElementoDefault{
				ElementoDefault: bson.ObjectId(ctls.ElementoDefault),
				ControlPeticion: ctrlpet,
			},
		}
		ObjetosCatalogos = append(ObjetosCatalogos, ObjetoCatalogo)
	}
	//Asignar los datos extraidos al documento de respuesta
	docSal.ObjetoCatalogo = &ObjetosCatalogos
	/*
		//Llenar la cabecera del documento
		docSal.Cabecera.ModuloOrigen = "Modulo de usuario"
		docSal.Cabecera.ModuloDestino = "Por el momento se desconoce"
		docSal.Cabecera.Fecha = time.Now()
		docSal.Cabecera.ServicioWeb = "/localhost:1025/Catalogos"

		//Llenar el control de errores
		docSal.ObjetoCatalogo.ControlPeticion.Estatus = true
		docSal.ObjetoCatalogo.ControlPeticion.Mensaje = "Exitoso"
		docSal.ObjetoCatalogo.ControlPeticion.Err = nil

		//Llenar los datos del documento (La estructura del objeto)
		docSal.ObjetoCatalogo.EstID.ID = "123456"
		docSal.ObjetoCatalogo.EstClave.Clave = 5
		docSal.ObjetoCatalogo.EstNombre.Nombre = "Juarez"
		docSal.ObjetoCatalogo.EstDescripcion.Descripcion = "Catalogo de preubas"
	*/

	/*
		ControlCalidad      controlCalidad
		EstID               estID
		EstClave            estClave
		EstNombre           estNombre
		EstDescripcion      estDescripcion
		EstCatalogoEditable estCatalogoEditable
		EstEstatusCatalogo  estEstatusCatalogo
		Elementos           []elementoMgo
		EstElementoDefault  estElementoDefault
		EstBitacora         []BitacoraElastic
	*/
	fmt.Println("Cabecera encriptado: ", docSal.Checksum)
	response.WriteEntity(docSal)
}

func obtenerCatalogo(request *restful.Request, response *restful.Response) {

	docSal := &CatalogoModel.DocumentoSalida{}

	//Llamar el método encriptado (puede ser otro webservice)
	//docSal.Checksum = "Encriptacion principal"

	//Llenar la cabecera del documento
	docSal.Cabecera.ModuloOrigen = "Modulo de usuario"
	docSal.Cabecera.ModuloDestino = "Por el momento se desconoce"
	docSal.Cabecera.Fecha = time.Now()
	docSal.Cabecera.ServicioWeb = "/localhost:1025/usuarios"

	/*
		//Llenar el control de errores
		docSal.ObjetoCatalogo.ControlPeticion.Estatus = true
		docSal.ObjetoCatalogo.ControlPeticion.Mensaje = "Exitoso"
		docSal.ObjetoCatalogo.ControlPeticion.Err = nil

		//Llenar los datos del documento (La estructura del objeto)
		docSal.ObjetoCatalogo.EstID.ID = "123456"
		docSal.ObjetoCatalogo.EstClave.Clave = 5
		docSal.ObjetoCatalogo.EstNombre.Nombre = "Juarez"
		docSal.ObjetoCatalogo.EstDescripcion.Descripcion = "Catalogo de preubas"
	*/
	/*
		ControlPeticion      ControlPeticion
		EstID               estID
		EstClave            estClave
		EstNombre           estNombre
		EstDescripcion      estDescripcion
		EstCatalogoEditable estCatalogoEditable
		EstEstatusCatalogo  estEstatusCatalogo
		Elementos           []elementoMgo
		EstElementoDefault  estElementoDefault
		EstBitacora         []BitacoraElastic
	*/

	response.WriteEntity(docSal)

}

/*
func FindUser(request *restful.Request, response *restful.Response) {
	id := request.PathParameter("user-id")
	// here you would fetch user from some persistence system
	usr := &user{Id: id, Name: "John Doe"}
	response.WriteEntity(usr)
}
*/

/*
func UpdateUser(request *restful.Request, response *restful.Response) {
	usr := new(user)
	err := request.ReadEntity(&usr)
	// here you would update the user with some persistence system
	if err == nil {
		response.WriteEntity(usr)
	} else {
		response.WriteError(http.StatusInternalServerError, err)
	}
}
*/

func obtenerInformacion(request *restful.Request) (CatalogoModel.DocumentoEntrada, error) {
	docEnt := CatalogoModel.DocumentoEntrada{}

	err := request.ReadEntity(&docEnt)
	return docEnt, err
}

func crearCatalogo(request *restful.Request, response *restful.Response) {
	docEnt, err := obtenerInformacion(request)
	if err != nil {
		response.WriteError(http.StatusInternalServerError, err)
	}

	if docEnt.InformacionCatalogo.ID != "" {
		docEnt.InformacionCatalogo.ID = bson.NewObjectId()
	}

	CatalogoModel.EstablecerCatalogo(CatalogoModel.CatalogoContexto{})
	ctlg := &CatalogoModel.InformacionCatalogo{
		ID:               docEnt.InformacionCatalogo.ID,
		Clave:            docEnt.InformacionCatalogo.Clave,
		Nombre:           docEnt.InformacionCatalogo.Nombre,
		Descripcion:      docEnt.InformacionCatalogo.Descripcion,
		ElementoDefault:  docEnt.InformacionCatalogo.ElementoDefault,
		EstatusCatalogo:  docEnt.InformacionCatalogo.EstatusCatalogo,
		CatalogoEditable: docEnt.InformacionCatalogo.CatalogoEditable,
	}

	docSal := &CatalogoModel.DocumentoSalida{}
	err = ctlg.CrearCatalogoMongo()
	if err == nil {
		//NO hay errores, por lo tanto se inserta en elasticsearch
		fmt.Println("NO hay errores, y se ha agregado el almacén")

		docSal.Cabecera = &CatalogoModel.Cabecera{
			ModuloOrigen:  "Catalogo",
			ModuloDestino: docEnt.Cabecera.ModuloOrigen,
			Fecha:         time.Now(),
			ServicioWeb:   "Ninguno",
		}
		docSal.Checksum = generarEncriptacion(docSal.Cabecera)
		/*
						docSal.ObjetoCatalofunc generarEncriptacion(estructura interface{}) [16]byte {
				jsonStr, err := json.Marshal(estructura)
				if err != nil {
					fmt.Println("error al convertir a bytes de json: ", err)
				}
				return md5.Sum(jsonStr)
			}go = &CatalogoModel.ObjetoCatalogo{
							ControlPeticion: &CatalogoModel.ControlPeticion{
								Estatus: true,
								Mensaje: "Actualizacion exitosa",
								Err:     nil,
							},
						}
		*/

		fmt.Println("Documento Saliente", docSal)
		fmt.Println("Cabecera encriptado: ", docSal.Checksum)
	} else {
		//Existen errores, y es necesario tratarlos aqui...
		fmt.Println("-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_")
		fmt.Println("Ocurrieron errores al momento de crear el catalogo", err)
		fmt.Println("_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+_-+")

		/*
				cab := &CatalogoModel.Cabecera{
					ModuloOrigen:  "Catalogo",
					ModuloDestino: docEnt.Cabecera.ModuloOrigen,
					Fecha:         time.Now(),
					ServicioWeb:   "Ninguno",
				}

				ctlPet := &CatalogoModel.ControlPeticion{
					Estatus: false,
					Mensaje: "No se pudo crear el objeto",
					Err:     err,
				}

				objCat := &CatalogoModel.ObjetoCatalogo{
					ControlCalidad: ctlPet,
				}

				docSal := &CatalogoModel.DocumentoSalida{
				Checksum: "SKIA928712KAJSAYJ1273",
				Cabecera: cab,
				ObjetoCatalogo: objtCat,
			}
		*/

		//docSal.Checksum = generarEncriptacion("125ASDFLASI")
		docSal.Cabecera = &CatalogoModel.Cabecera{
			ModuloOrigen:  "Catalogo",
			ModuloDestino: docEnt.Cabecera.ModuloOrigen,
			Fecha:         time.Now(),
			ServicioWeb:   "Ninguno",
		}
		docSal.Checksum = generarEncriptacion(docSal.Cabecera)
		/*
			docSal.ObjetoCatalogo = &CatalogoModel.ObjetoCatalogo{
				ControlPeticion: &CatalogoModel.ControlPeticion{
					Estatus: false,
					Mensaje: "No se pudo crear el objeto",
					Err:     err,
				},
			}
		*/

		fmt.Println("Documento Saliente con error", docSal)
		fmt.Println("Cabecera encriptado: ", docSal.Checksum)
	}
	//Responder a la peticion con los datos apropiados
	response.Header().Set("Content-Type", "application/json")
	fmt.Fprint(response, docSal)
}

func generarEncriptacion(estructura interface{}) [16]byte {
	jsonStr, err := json.Marshal(estructura)
	if err != nil {
		fmt.Println("error al convertir a bytes de json: ", err)
	}
	return md5.Sum(jsonStr)
}

func RemoveUser(request *restful.Request, response *restful.Response) {
	// here you would delete the user from some persistence system
}

/*
//servicioWeb contiene la url del servicio rest
var servicioWeb string

//cabecera Cabecera del documento a enviar/recibir
type cabecera struct {
	ModuloOrigen  string
	ModuloDestino string
	Fecha         time.Time
	ServicioWeb   string
}

//Estructura del documento a enviar
type documentoEntrada struct {
	Cabecera            cabecera
	InformacionCatalogo informacionCatalogo
}

type informacionCatalogo struct {
	ID               bson.ObjectId `bson:"_id,omitempty"`
	Clave            int           `bson:"Clave"`
	Nombre           string        `bson:"Nombre"`
	Descripcion      string        `bson:"Descripcion"`
	CatalogoEditable bool          `bson:"Editable"`
	EstatusCatalogo  string        `bson:"Estatus"`
	Elementos        []elementoMgo `bson:"Elementos"`
	ElementoDefault  string        `bson:"ElementoDefault,omitempty"`
}

//ElementoMgo subestructura de CatalogoNuevo
type elementoMgo struct {
	SubClave      string `bson:"SubClave"`
	Valor         string `bson:"Valor"`
	ValorEditable bool   `bson:"Editabe"`
	EstatusValor  string `bson:"Estatus"`
}

////////////////////////////////////
//Estructura del documento a recibir
///////////////////////////////////
type controlCalidad struct {
	Estatus bool
	Mensaje string
	Err     error
}

type documentoSalida struct {
	Checksum       string
	Cabecera       cabecera
	ObjetoCatalogo objetoCatalogo
}

type objetoCatalogo struct {
	ControlCalidad      controlCalidad
	EstID               estID
	EstClave            estClave
	EstNombre           estNombre
	EstDescripcion      estDescripcion
	EstCatalogoEditable estCatalogoEditable
	EstEstatusCatalogo  estEstatusCatalogo
	Elementos           []estElementoMgo
	EstElementoDefault  estElementoDefault
}

//Estructuras con estatus detallados
type estID struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	ControlCalidad controlCalidad
}

type estClave struct {
	Clave          int `bson:"Clave"`
	ControlCalidad controlCalidad
}

type estNombre struct {
	Nombre         string `bson:"Nombre"`
	ControlCalidad controlCalidad
}

type estDescripcion struct {
	Descripcion    string `bson:"Descripcion"`
	ControlCalidad controlCalidad
}

type estCatalogoEditable struct {
	CatalogoEditable bool `bson:"Editable"`
	ControlCalidad   controlCalidad
}

type estEstatusCatalogo struct {
	EstatusCatalogo string `bson:"Estatus"`
	ControlCalidad  controlCalidad
}

type estElementoDefault struct {
	ElementoDefault bson.ObjectId `bson:"ElementoDefault,omitempty"`
	ControlCalidad  controlCalidad
}

//ElementoMgo subestructura de CatalogoNuevo
type estElementoMgo struct {
	EstSubClave      estSubClave
	EstValor         estValor
	EstValorEditable estValorEditable
	EstEstatusValor  estEstatusValor
}

type estSubClave struct {
	SubClave       string `bson:"SubClave"`
	ControlCalidad controlCalidad
}

type estValor struct {
	Valor          string `bson:"Valor"`
	ControlCalidad controlCalidad
}

type estValorEditable struct {
	ValorEditable  string `bson:"Editabe"`
	ControlCalidad controlCalidad
}

type estEstatusValor struct {
	EstatusValor   string `bson:"Estatus"`
	ControlCalidad controlCalidad
}

*/
