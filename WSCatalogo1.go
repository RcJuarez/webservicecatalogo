package main

import (
	"fmt"

	restful "github.com/emicklei/go-restful"

	"./CatalogoModel"
	"./Modulos/Variables"

	iris "gopkg.in/kataras/iris.v6"
	"gopkg.in/kataras/iris.v6/adaptors/httprouter"
)

func main() {
	app := iris.New()
	app.Adapt(httprouter.New())
	app.Set(iris.OptionCharset("UTF-8"))

	var DataCfg = MoVar.CargaSeccionCFG(MoVar.SecDefault)

	//###################### Catalogo ################################

	//################# MÉTODOS DE CONSULTA ##########################

	app.Post("/Catalogos", GetCatalogo)

	//################### MÉTODOS DE CRUD ############################

	//################# MÉTODOS DE CONSUMO ###########################

	if DataCfg.Puerto != "" {
		fmt.Println("Ejecutandose en el puerto: ", DataCfg.Puerto)
		fmt.Println("Acceder a la siguiente url: ", DataCfg.BaseURL)
		app.Listen(":" + DataCfg.Puerto)
	} else {
		fmt.Println("Ejecutandose en el puerto: 1025")
		fmt.Println("Acceder a la siguiente url: localhost")
		app.Listen(":1025")
	}

}

func GetCatalogo(ctx *iris.Context) {
	fmt.Println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
	fmt.Println(ctx.FormValues())
	/*
		docEnt, err := obtenerInformacion(ctx.Request)
		if err != nil {
			fmt.Println("xxxxxxxxxxxxxxxxx", err, docEnt)
		}

		//Inicializar la base de datos
		MoConexion.InitData()
		//Establecer el contexto a usar
		CatalogoModel.EstablecerCatalogo(CatalogoModel.CatalogoContexto{})
		//Crear un nuevo contexto
		ctx, err := MoConexion.NuevoContexto()
		//validar el error del contexto
		if err != nil {
			fmt.Println("Ocurrieron errores al crear un nuevo contexto", err)
		} else {
			//Cerrar la copia de la conexion
			defer ctx.Cerrar()
		}
		//Crear la referencia del catalogo (paa acceder a los metodos de la interfaz)
		ctlg := &CatalogoModel.InformacionCatalogo{}
		//Acceder al método para obtener los catalogos
		catalogos, err := ctlg.ObtenerCatalogosMongo()
		if err != nil {
			fmt.Println("Error al consultar los catalogos", err)
		} else {
			fmt.Println("Catalogos encontrados: ", catalogos)
		}

		//Rellenar el documento de respuesta

		//Establecer la cabecera
		cab := &CatalogoModel.Cabecera{
			ModuloOrigen:  "Catalogo",
			ModuloDestino: docEnt.Cabecera.ModuloOrigen,
			Fecha:         time.Now(),
			ServicioWeb:   "/localhost:1025/Catalogos",
		}

		//Crear una referencia a la estructura de documento salida
		docSal := &CatalogoModel.DocumentoSalida{}
		//Asignar la cabecera
		docSal.Cabecera = cab
		//Asignar la encriptacion
		docSal.Checksum = generarEncriptacion(docSal.Cabecera)
		//Crear el control de la peticion del objeto
		ctrlpet := &CatalogoModel.ControlPeticion{
			Estatus: true,
			Mensaje: "Exitoso",
			Err:     nil,
		}
		//Crear un arreglo de catalogos
		ObjetosCatalogos := []CatalogoModel.ObjetoCatalogo{}
		//Tratar los datos extraidos
		for _, ctls := range *catalogos {
			ObjetoCatalogo := CatalogoModel.ObjetoCatalogo{
				ControlPeticion: ctrlpet,
				EstID: &CatalogoModel.EstID{
					ID:              ctls.ID,
					ControlPeticion: ctrlpet,
				},
				EstClave: &CatalogoModel.EstClave{
					Clave:           ctls.Clave,
					ControlPeticion: ctrlpet,
				},
				EstNombre: &CatalogoModel.EstNombre{
					Nombre:          ctls.Nombre,
					ControlPeticion: ctrlpet,
				},
				EstDescripcion: &CatalogoModel.EstDescripcion{
					Descripcion:     ctls.Descripcion,
					ControlPeticion: ctrlpet,
				},
				EstCatalogoEditable: &CatalogoModel.EstCatalogoEditable{
					CatalogoEditable: ctls.CatalogoEditable,
					ControlPeticion:  ctrlpet,
				},
				EstEstatusCatalogo: &CatalogoModel.EstEstatusCatalogo{
					EstatusCatalogo: ctls.EstatusCatalogo,
					ControlPeticion: ctrlpet,
				},
				EstElementoDefault: &CatalogoModel.EstElementoDefault{
					ElementoDefault: bson.ObjectId(ctls.ElementoDefault),
					ControlPeticion: ctrlpet,
				},
			}
			ObjetosCatalogos = append(ObjetosCatalogos, ObjetoCatalogo)
		}
		//Asignar los datos extraidos al documento de respuesta
		docSal.ObjetoCatalogo = &ObjetosCatalogos

		fmt.Println("Cabecera encriptado: ", docSal.Checksum)
	*/
	type docsal struct {
		doc1 string
		doc2 int
	}

	doc := &docsal{
		doc1: "hola",
		doc2: 5,
	}
	err := ctx.JSON(iris.StatusOK, doc)
	if err != nil {
		ctx.Write([]byte("Ocurrió un problema al decodificar 2"))
		return
	}
}

func obtenerInformacion(request *restful.Request) (CatalogoModel.DocumentoEntrada, error) {
	docEnt := CatalogoModel.DocumentoEntrada{}

	err := request.ReadEntity(&docEnt)
	return docEnt, err
}
